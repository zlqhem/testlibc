#include "CppUTest/TestHarness.h"

extern "C"
{
	#include <stdio.h>
	#include "com.lge.testdesign/TestDesign.h"
	#include "com.lge.testdesign.partition/EquivalencePartition.h"
}

static BOOL checkPartitionClassExist(Set* partitions, int value);

TEST_GROUP(EquivalencePartitionLibTest)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};

/*
 * Test Case 1 : (1~10), (11~20) 조건을 입력 하였을 때 Test Condition 생성
 * Objective : 입력 값 기준 Test Condition 생성 되는지를 확인
 * input : (1~10), (11~20)
 * output : (0~10), (11~20), (< 0), (20 <)
 */
TEST(EquivalencePartitionLibTest, testDeriveTestCondition)
{
	/*
	 * Step 1. test basis 정의
	 */
	int startValue1 = 0;
	int endValue1 = 10;
	int startValue2 = 11;
	int endValue2 = 20;

	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */
	EPDeriveTestConditions* conditionGen = New_EPDeriveTestConditions();
	EPDeriveTestConditions_putIntRange(conditionGen, startValue1, endValue1);
	EPDeriveTestConditions_putIntRange(conditionGen, startValue2, endValue2);


	/*
	 * 결과 값 비교
	 */
	// set of PartitionClass
	Set* conditions = DeriveTestConditions_genTestConditions((DeriveTestConditions*)conditionGen);

	CHECK_TRUE(0 != conditions);
	CHECK_EQUAL(3, Set_length(conditions)); // { (0..10), (11..20), otherwise }

	int input[3] = {5, 15, -10};
	CHECK_TRUE(checkPartitionClassExist(conditions, 5));	// (0..10)
	CHECK_TRUE(checkPartitionClassExist(conditions, 15));	// (11..20)
	CHECK_TRUE(checkPartitionClassExist(conditions, -15));	// otherwise
}

static BOOL checkPartitionClassExist(Set* partitions, int value)
{
	int len = partitions->len;
	BOOL found = FALSE;
	for (int i=0; i<len; i++)
	{
		PartitionClass* cond = (PartitionClass*)(partitions->data[i]);

		if (TRUE == PartitionClass_contains(cond, (void*)value))
		{
			found = TRUE;
		}
	}

	if (FALSE == found)
	{
		return FALSE;
	}
	return TRUE;
}

/*
 * Test Case 2 : (0~10) 조건을 입력 하고, 전체 입력 값을 {-10~30}로 하였을 때  Test Coverage Items 생성
 * Objective : 입력 값 기준 Test Coverage Items 생성 되는지를 확인
 * input : 조건 = (0~10), 입력 값 = {-10~30}
 * output : (0~10) Item 존재, !(0~10) Item 존재
 */
TEST(EquivalencePartitionLibTest, testDeriveTestCoverageItem)
{
	/*
	 * Step 1. test basis 정의
	 */
	int startValue = 0;
	int endValue = 10;
//	List* universe = makeIntegerList(-10,30);

	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */
	EPDeriveTestConditions* conditionGen = New_EPDeriveTestConditions();
	EPDeriveTestConditions_putIntRange(conditionGen, startValue, endValue);

	/*
	 * Step3. derive test coverage items (TD3)
	 */
	EPDeriveTestCoverageItems* coverageItemgen = New_EPDeriveTestCoverageItems();

	/*
	 * 결과 값 비교
	 */
	Set* conditions = EPDeriveTestConditions_genTestConditions(conditionGen);
	Set* coverageItems = EPDeriveTestCoverageItems_genTestCoverageItems(coverageItemgen, conditions);

//	CHECK_TRUE(NULL != coverageItems);
//	CHECK_EQUAL(2, Set_length(coverageItems));
}

