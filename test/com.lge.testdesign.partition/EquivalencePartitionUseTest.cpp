/*
 * EquivalencePartitionUseTest.cpp
 *
 *      Author: junhee.cho
 */
#include "CppUTest/TestHarness.h"

extern "C"
{
	#include <stdio.h>
	#include "com.lge.testdesign/TestDesign.h"
	#include "com.lge.testdesign.partition/EquivalencePartition.h"
}

static BOOL checkPartitionClassExist(Set* partitions, int value);

TEST_GROUP(EquivalencePartitionUseTest)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};

/*
 * ISO29119-4 B.2.1 Equivalence Partitioning example (p.50)
 */
TEST(EquivalencePartitionUseTest, ISO29119_Example)
{
	/*
	 * 정의 및 원리
	 *   테스트 아이템의 입력 또는 출력이 여러 영역으로 구분되는 경우에 적용
	 *   동일한 영역 내에서 어떠한 값을 선택해도 항상 같은 결과를 기대할 수 있다는 것을 전제로 함
	 *
	 * 예제 설명
	 *   - 시스템 명: 교과목 학점 평가 시스템
	 *   - 요구사항
	 *     1. 시험 점수(75%) + 과제 점수(25%) = 총점 = 학점 계산(A,B,C,D)
	 *     2. 시험 점수 입력: 0 ~ 75 점
	 *     3. 과제 점수 입력: 0 ~ 25 점
	 *     4. 점수(시험, 과제) 별 범위를 벗어나는 점수를 입력하면 "경고 메시지"출력
	 *     5. 정수만 입력으로 가정
	 *
	 *   +----------------------+
	 *   | Total        | grade |
	 *   +--------------+-------+
	 *   | 70 <= ~      |   A   |
	 *   | 50 <= ~ < 70 |   B   |
	 *   | 30 <= ~ < 50 |   C   |
	 *   | ~ < 30       |   D   |
	 *   +--------------+-------+
	 */

	/*
	 * Step 1. test basis 정의
	 */


	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */


	/*
	 * Step3. derive test coverage items (TD3)
	 */

}
