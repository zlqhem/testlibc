#include "CppUTest/TestHarness.h"

extern "C"
{
	#include <stdio.h>
	#include "com.lge.testdesign/TestDesign.h"
	#include "com.lge.testdesign.boundary/BoundaryValue.h"
}

TEST_GROUP(BoundaryValuePartitionLib)
{
	void setup()
	{
	}
	
	void teardown()
	{
	}
};

static void Set_PrintInt(Set* set)
{
	for (int i=0; i<set->len; i++)
	{
		printf ("%d ", set->data[i]);
	}
	puts ("");
}

TEST(BoundaryValuePartitionLib, testUsingTestDesignClass) {
	/*
	 * Step 1. test basis 정의
	 */
	int basis = 0;

	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */
	BoundaryValueDeriveTestConditions* conditionGen =
			New_BoundaryValueDeriveTestConditions();
	BoundaryValueDeriveTestConditions_put(conditionGen, (void*) basis);

	/*
	 * Step3. derive test coverage items (TD3)
	 */
	BoundaryValueDeriveTestCoverageItems* coverageItemgen =
			New_IntegerBoundaryValueDeriveTestCoverageItems();

	/*
	 * 결과 값 비교
	 */
	Set* testConditions = BoundaryValueDeriveTestConditions_genTestConditions(conditionGen);
	Set* expected = BoundaryValueDeriveTestCoverageItems_genTestCoverageItems(
						coverageItemgen, testConditions);

	TestDesign tdObj;
	TestDesign* td = New_TestDesign(&tdObj,
								   (DeriveTestConditions*) conditionGen,
								   (DeriveTestCoverageItems*) coverageItemgen);

	Set* actual = TestDesign_deriveCoverageItems(td);

	CHECK_TRUE(Set_equals(expected, actual));

}
