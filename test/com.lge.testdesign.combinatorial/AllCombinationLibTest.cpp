#include "CppUTest/TestHarness.h"

extern "C"
{
	#include <stdio.h>
	#include "com.lge.testdesign/TestDesign.h"
	#include "com.lge.testdesign.combinatorial/Combinatorial.h"
}


TEST_GROUP(AllCombinationLibTest)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};


/*
	 * Test Case 1 : 한자리 = {0,1}, 세자리 = {100,111} 을 입력 하였을 때 Test Condition 생성
	 * Objective : 입력 값 기준 Test Condition 생성 되는지를 확인
	 * input : P1 = {0,1}, P2 = {100,111}
	 * output : P1 = 0, P1 = 1, P2 = 100, P2 = 111
	 */

TEST(AllCombinationLibTest, testDeriveTestConditions)
{
	// 도출되는 Test Condition 은 조합기법(all/each choice/pairwise)과 관계없이 같음.

	/*
	 * Step 1. test basis 정의
	 */
	Parameter* p1 =  New_ParameterClass("p1");
	addValue(p1, (void*)0);
	addValue(p1, (void*)1);

	Parameter* p2 =  New_ParameterClass("p2");
	addValue(p2, (void*)100);
	addValue(p2, (void*)111);


	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */
	CombinationDeriveTestConditions* conditionGen = New_CombinationDeriveTestConditions();
	CombinationDeriveTestConditions_put(conditionGen, p1);
	CombinationDeriveTestConditions_put(conditionGen, p2);

	/*
	 * 결과 값 비교
	 */
	Set* actual = DeriveTestConditions_genTestConditions((DeriveTestConditions*)conditionGen);
	Set* expected = NULL;

	Set_equals_cmp(expected, actual, ParameterValuePairContainer_equal);

}

/*
	 * Test Case 2 : 한자리 = {0,1}, 세자리 = {100,111} 을 입력 하였을 때 Test Coverage Items 생성
	 * Objective : 입력 값 기준 Test Coverage Items 생성 되는지를 확인
	 * input : P1 = {0,1}, P2 = {100,111}
	 * output : {P1: 0, P2: 100}, {P1: 1, P2: 100}, {P1: 0, P2: 111}, {P1: 1, P2: 111}
	 */
TEST(AllCombinationLibTest, testDeriveTestCoverageItems)
{
	/*
	 * Step 1. test basis 정의
	 */
	Parameter* p1 =  New_ParameterClass("p1");
	addValue(p1, (void*)0);
	addValue(p1, (void*)1);

	Parameter* p2 =  New_ParameterClass("p2");
	addValue(p2, (void*)100);
	addValue(p2, (void*)111);


	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */
	CombinationDeriveTestConditions* conditionGen = New_CombinationDeriveTestConditions();
	CombinationDeriveTestConditions_put(conditionGen, p1);
	CombinationDeriveTestConditions_put(conditionGen, p2);

	/*
	 * Step3. deriveTestCoverageItems Constructor (TD3)
	 */


	/*
	 * 결과 값 비교
	 */
	Set* conditions = DeriveTestConditions_genTestConditions((DeriveTestConditions*)conditionGen);

}
