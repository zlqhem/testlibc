#include "CppUTest/TestHarness.h"

TEST_GROUP(CombinatorialUseTest)
{
	void setup()
	{
	}

	void teardown()
	{
	}
};

TEST(CombinatorialUseTest, AllCombination)
{
	/*
	 * 정의 및 원리
	 *   입력 값을 조합해 테스트 케이스를 생성
	 *   다른 기법을 이용해 테스트 데이터(입력 값)을 선정한 후 활용 가능(예: 동등 분할, 경계 값 분석)
	 *
	 * 예제 설명
	 *   - 시스템 명: 항공 여행 안내 시스템
	 *   - 요구사항
	 *     1. 목적지 = 도쿄, 상하이, 방콕, 파리, 런던, 로마, 뉴욕, 시드니
	 *     2. 좌석등급 = 퍼스트 클래스, 비지니스 클래스, 이코노미 클래스
	 *     3. 좌석위치 = 통로, 창가
	 *
	 */
	/*
	 * Step 1. test basis 정의
	 */


	/*
	 * Step2. deriveTestConditions Constructor (TD2)
	 */


	/*
	 * Step3. deriveTestCoverageItems Constructor (TD3)
	 */

}
