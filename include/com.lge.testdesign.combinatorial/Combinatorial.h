#ifndef __COMBINATORIAL_H__
#define __COMBINATORIAL_H__

#include "utils/common.h"
#include "utils/Set.h"
#include "utils/uthash.h"
#include "com.lge.testdesign/TestDesign.h"

typedef struct CombinationDeriveTestCoverageItems {
	DeriveTestCoverageItems base;
	List* params;
} CombinationDeriveTestCoverageItems;

typedef struct Parameter Parameter;
typedef struct CombinationDeriveTestConditions CombinationDeriveTestConditions;
typedef struct CombinationDeriveTestCoverageItems CombinationDeriveTestCoverageItems;
typedef struct ParameterValuePairContainer ParameterValuePairContainer;

/*
 * Parameter operations
 */
extern Parameter* New_ParameterClass(const char* name);
extern void	addValue(Parameter* parameter, void* data);
extern int length(Parameter* parameter);

/*
 * CombinationDeriveTestConditions
 */
extern CombinationDeriveTestConditions* New_CombinationDeriveTestConditions();
extern void CombinationDeriveTestConditions_put(CombinationDeriveTestConditions* conds, void* parameter);
extern Set* CombinationDeriveTestConditions_genTestConditions(CombinationDeriveTestConditions*);

/*
 * CombinationDeriveTestCoverageItems
 */
// CombinationDeriveTestCoverageItems
extern CombinationDeriveTestCoverageItems* New_AllCombinationDeriveTestCoverageItems(List* params);
extern Set* CombinationDeriveTestConditions_genTestCoverageItems(
		CombinationDeriveTestCoverageItems*, Set* testConditions);

/*
 * ParameterValuePairContainer
 */
extern ParameterValuePairContainer* New_ParameterValuePairContainer(const char* name, void* v);
extern int ParameterValuePairContainer_equal(void* a, void* b);

#endif
