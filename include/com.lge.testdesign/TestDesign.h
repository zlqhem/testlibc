#ifndef __TEST_DESIGN__
#define __TEST_DESIGN__

#include "utils/Set.h"

typedef struct DeriveTestConditions{
	Set* (*genTestConditions)(struct DeriveTestConditions*);
} DeriveTestConditions;

typedef struct DeriveTestCoverageItems {
	Set* (*genTestCoverageItems)(struct DeriveTestCoverageItems*, Set* testConditions);
} DeriveTestCoverageItems;

typedef struct TestDesign {
	DeriveTestConditions* deriveTestConditions;
	DeriveTestCoverageItems* deriveTestCoverageItems;
} TestDesign;

extern TestDesign* New_TestDesign(TestDesign*, DeriveTestConditions*, DeriveTestCoverageItems*);

extern Set* TestDesign_generateTestConditions(TestDesign*);
extern Set* TestDesign_generateCoverageItems(TestDesign*, Set* testConditions);
extern Set* TestDesign_deriveCoverageItems(TestDesign*);

extern Set* DeriveTestConditions_genTestConditions(DeriveTestConditions* conds);

#endif /* __TEST_DESIGN__ */
