#ifndef __BoundaryValue_h__
#define __BoundaryValue_h__

#include "utils/Set.h"
#include "com.lge.testdesign/TestDesign.h"

typedef struct BoundaryValueDeriveTestConditions {
	DeriveTestConditions base;
	Set* boundaries;
} BoundaryValueDeriveTestConditions;

typedef struct BoundaryValueDeriveTestCoverageItems {
	DeriveTestCoverageItems base;
} BoundaryValueDeriveTestCoverageItems;

/*
 * DeriveTestConditions
 */
extern BoundaryValueDeriveTestConditions* New_BoundaryValueDeriveTestConditions();
extern void BoundaryValueDeriveTestConditions_put(BoundaryValueDeriveTestConditions*, void*);
extern Set* BoundaryValueDeriveTestConditions_genTestConditions(BoundaryValueDeriveTestConditions*);

/*
 * DeriveTestCoverageItems
 */
extern BoundaryValueDeriveTestCoverageItems* New_BoundaryValueDeriveTestCoverageItems();
extern BoundaryValueDeriveTestCoverageItems* New_IntegerBoundaryValueDeriveTestCoverageItems();
extern Set* BoundaryValueDeriveTestCoverageItems_genTestCoverageItems(
		BoundaryValueDeriveTestCoverageItems*, Set* testConditions);


#endif /* __BoundaryValue.h__ */
