#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define ALLOC(T)	(T*)calloc(1, sizeof(T))
#define CAST_INT(ptr)	*((int*)&(ptr))

#define MAX_INPUT_STR_LEN 300

typedef int BOOL;

typedef struct _List {
	void*			data;
	struct _List* 	next;
} List;

typedef struct _String {
    char str[MAX_INPUT_STR_LEN];
} String;

typedef struct Pair {
	void* first;
	void* second;
} Pair;

Pair* New_Pair(void* first, void* second);

List*	List_Create(void* data);
List*	List_Append(List*, void* data);
List*	List_Concat(List*, List*);
void 	List_Free(List*);
int     List_Length(List*);
void    List_Iterate(List*, void (*apply_function)(void*));

String* String_Clone(const char* s);
String* String_Clone_Int(int);
String* String_CopyRaw(String* out, const char* s);
String* String_Copy(String* out, const String* s);
String* String_CopyInt(String* out, int v);
int     String_ToInt(const String*);
void    String_Free(String* s);
int     String_CompareTo(const String* s, const String* s2);
int     String_CompareToRaw(const String* s, const char* s2);
int     String_Strlen(String* s);
BOOL    String_IsEmpty(const String* s);

extern  String* EmptyString;

#endif /* __COMMON_H__ */
