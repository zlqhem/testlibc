#ifndef __SET_H__
#define __SET_H__

#include "common.h"

typedef struct Set {
	char*	data[100];
	int		len;
} Set;

extern Set* New_Set();
extern void Set_put(Set*, void* elem);	// deprecated
extern void Set_add(Set*, void* elem);
extern BOOL Set_isEmpty(Set* set);
extern void Set_addAll(Set* dest, Set* src);
extern BOOL Set_equals(Set*, Set*);
extern BOOL Set_equals_cmp(Set* src, Set* target, int (*cmp)(void*, void*));
extern int Set_length(Set*);

#endif /* __SET_H__ */
