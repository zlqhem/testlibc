#ifndef __EquivalencePartition_h__
#define __EquivalencePartition_h__

#include "utils/common.h"
#include "utils/Set.h"
#include "com.lge.testdesign/TestDesign.h"

typedef struct PartitionClass PartitionClass;
typedef struct EPDeriveTestConditions EPDeriveTestConditions;
typedef struct EPDeriveTestCoverageItems EPDeriveTestCoverageItems;

typedef struct PartitionClass {
	void* env;	/* environment when the `contains` function is called */
	BOOL (*contains)(void* env, void* elem);
} PartitionClass;

/*
 * PartitionClass operations
 */
extern PartitionClass* New_PartitionClass(void* env, BOOL (*contains)(void* env, void* elem));
extern PartitionClass* New_PartitionClass_Range(int start, int end);
extern BOOL PartitionClass_contains(PartitionClass*, void* element);

/*
 * PartitionOp operations
 */
extern PartitionClass* PartitionOp_or(PartitionClass* a, PartitionClass* b);
extern PartitionClass* PartitionOp_and(PartitionClass* a, PartitionClass* b);
extern PartitionClass* PartitionOp_negate(PartitionClass* basis);
extern PartitionClass* PartitionOp_nothing();

/*
 * DeriveTestConditions
 */
extern EPDeriveTestConditions* New_EPDeriveTestConditions();
extern void EPDeriveTestConditions_put(EPDeriveTestConditions* conds, PartitionClass* pc);
extern void EPDeriveTestConditions_putIntRange(EPDeriveTestConditions* conds, int start, int low);
// set of PartitionClass
extern Set* EPDeriveTestConditions_genTestConditions(EPDeriveTestConditions*);

/*
 * DeriveTestCoverageItems
 */
extern EPDeriveTestCoverageItems* New_EPDeriveTestCoverageItems();
extern Set* EPDeriveTestCoverageItems_genTestCoverageItems(
		EPDeriveTestCoverageItems* coverageItemgen, Set* testConditions);

#endif /* __EquivalencePartition_h__ */
