#include "CrestTestHarness.h"
#include "CrestTestMacro.h"

static void printFailureMessage(const char *msg);
static void printFailureMessageToConsole(const char *msg);
static void printFailureMessageToFile(const char *msg);

extern "C" {
	#include <setjmp.h>
        #include "PlatformSpecificCrestTestFunctions.h"

        void __CrestInit();

	#define TEST_SUCCESS 0
	#define TEST_FAIL 1
	static jmp_buf test_jmp_buf;
	static int test_result = TEST_FAIL;	 

	void PlatformSpecificCrestLongJmp()
	{
		longjmp(test_jmp_buf, 1);
	}

	void assertTrue(int failed, const char * checkString, const char* conditionString, const char* fileName, int lineNumber) 
	{
		if (failed) {
                	char message[256];
                	snprintf (message, 256, "%s:%d: error: Failure\n\t%s (%s) failed\n", fileName, lineNumber, checkString, conditionString);
                	printFailureMessage(message);
			test_result = TEST_FAIL;
			PlatformSpecificCrestLongJmp();
		}
	}

	void assertEquals(int failed, long int expected, long int actual, const char* file, int line) 
	{
		if (failed) {
		        char message[256];
		        snprintf (message, 256, "%s:%d: error: Failure\n\texpected <0x%ld>\n\tbut was  <0x%ld>\n", file, line, (long int)expected, (long int)actual);
                	printFailureMessage(message);
			test_result = TEST_FAIL;
			PlatformSpecificCrestLongJmp();
		}
	}
}

static void printFailureMessage(const char *msg)
{
    printFailureMessageToConsole(msg);
    printFailureMessageToFile(msg);
}

static void printFailureMessageToConsole(const char *msg)
{
    fprintf (stderr, msg);
}

static void printFailureMessageToFile(const char *msg)
{
    char *fname = 0;
    if (fname = getenv(ENV_STRING_CREST_ERROR_LOG)) {
        FILE* fd = fopen (fname, "w");
        if (fd) {
            fprintf (fd, msg);
            fclose (fd);
        }
    }
}


/////////////////  Definitions 
CrestConfig& CrestConfig::instance()
{
	static CrestConfig _instance("run_crest", "dfs", 1000000);
	return _instance;
}

CrestConfig::CrestConfig(const char* crest_path, const char *strategy, unsigned int iter):
		crest_path_(crest_path), strategy_(strategy), maxIteration_(iter) {}

CrestTestRunner::CrestTestRunner():
	crest_path_(0), xmloutpath_("undefinedFile"), maxIteration_(0), strategy_(""),
	entry_(0), cpputestname_("undefinedTestName")
{
}

CrestTestRunner::~CrestTestRunner(){}

void CrestTestRunner::setMaxIteration(unsigned n) { maxIteration_ = n; }
void CrestTestRunner::setStrategy(const char *s) { strategy_ = s; }
void CrestTestRunner::setCrestPath(const char *run_crest) { crest_path_ = run_crest; }

int CrestTestRunner::run(bool isReplayMode = false) 
{
	setupRunner();
	if (shouldRun(isReplayMode)) {
		test_result = TEST_SUCCESS;
		if (0 == setjmp(test_jmp_buf)) {
			test_result = runCrestTestMain();
		}

		if (TEST_FAIL == test_result) {
		    writeStopFile();
		    // pass abnormal exit status to run_crest
		    exit(1);
		}

		return test_result;
	}
	else {
		return runCrest();
	}
}

void CrestTestRunner::setupRunner()
{
}

void CrestTestRunner::writeStopFile()
{
    const char* file = getenv("CREST_STOP");
    if (file) {
        FILE* fd = fopen(file, "w");
        fclose(fd);
    }
}

int CrestTestRunner::runCrestTestMain()
{
	// __CrestInit 은 instrumentation 를 통해 main 함수 시작 지점에서 호출되는 함수.
	// 그러나 main 함수가 있는 AllTests.cpp 파일은 CREST 의 
	// instrumenation 빌드에 포함되지 않기 때문에 __CrestInit 함수호출이
	// 어디에도 없게 된다. 명시적으로 호출 할 필요가 있음.
	__CrestInit();
	return entry_();
}

bool CrestTestRunner::shouldRun(bool isReplayMode) {
	bool shouldCallEntry = getenv(ENV_STRING_ENTRY_CALL_MODE) != 0;
	bool enable = shouldCallEntry || isReplayMode;

	return entry_ != 0 && enable;
}

int CrestTestRunner::runCrest()
{
	char cmd[2048];
	const char *env_flag = ENV_STRING_ENTRY_CALL_MODE;
	char exename[256];
	char envset[128];

        PlatformSpecific_CommandLine_EnvSet(envset, 128, ENV_STRING_ENTRY_CALL_MODE, "1");
        PlatformSpecific_GetCurrentExePath(exename, 256);

	fprintf (stderr, "[COMMAND] %s %s \"%s -n %s\" %u -%s --junit-xml=%s --junit-group=%s",
			envset, crest_path_, exename, cpputestname_, maxIteration_, strategy_, xmloutpath_, cpputestname_);
	sprintf (cmd, "%s %s \"%s -n %s\" %u -%s --junit-xml=%s --junit-group=%s",
			envset, crest_path_, exename, cpputestname_, maxIteration_, strategy_, xmloutpath_, cpputestname_);

	return system(cmd);			
}

CrestConfigInstaller::CrestConfigInstaller(const char *crest_path, const char *strategy, unsigned int iterMax)
{
	CrestConfig &currentConfig = CrestConfig::instance();
	currentConfig.crest_path_ = crest_path;
	currentConfig.strategy_ = strategy;
	currentConfig.maxIteration_ = iterMax;
}
CrestTestInstaller::~CrestTestInstaller() {} 

CrestTestInstaller::CrestTestInstaller(
	CrestTestRunner& runner, const char *xmlout, 
	int (*crest_main)(), const char *testname)
{
	CrestConfig &defaultConfig = CrestConfig::instance();
	runner.crest_path_ = defaultConfig.crest_path_;
	runner.xmloutpath_ = xmlout;
	runner.maxIteration_ = defaultConfig.maxIteration_;
	runner.strategy_ = defaultConfig.strategy_;
	runner.entry_ = crest_main;
	runner.cpputestname_ = testname;
}


