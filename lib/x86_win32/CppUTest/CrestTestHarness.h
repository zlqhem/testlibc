<<<<<<< HEAD
#ifndef __CREST_TEST_HARNESS_H__
#define __CREST_TEST_HARNESS_H__

#include <stdio.h>
#include "TestHarness.h"

#define CREST_STRATEGY_DFS "dfs"
#define CREST_STRATEGY_CFG "cfg"
#define CREST_STRATEGY_RANDOM "random"

#define ENV_STRING_ENTRY_CALL_MODE "__ENABLE_CPPUTEST_CREST"
#define ENV_STRING_CREST_STOP "CREST_STOP"
#define ENV_STRING_CREST_ITERATION "CREST_ITERATION"
#define ENV_STRING_CREST_ERROR_LOG "CREST_ERROR_LOG"

extern "C" {
	#include <stdlib.h>	// system, getenv
	extern void __CrestInit(void);
}

class CrestConfig {
  public:
  	static CrestConfig& instance();
	virtual ~CrestConfig() {}
	const char *crest_path_;
	const char *strategy_;
	unsigned int maxIteration_;	
	
  private:
	CrestConfig(const char* crest_path, const char *strategy, unsigned int iter);

};

class CrestConfigInstaller {
  public:
	CrestConfigInstaller(const char *crest_path, const char *strategy, unsigned int iterMax);
	virtual ~CrestConfigInstaller() {};
};

class CrestTestRunner {
  public:
	CrestTestRunner();
	virtual ~CrestTestRunner();
	const char *crest_path_;
	const char *xmloutpath_;
	unsigned int maxIteration_;
	const char *strategy_;
	int (*entry_)();
   	const char *cpputestname_;
	void setMaxIteration(unsigned int n);
	void setStrategy(const char *s);
	void setCrestPath(const char *s);	
	int run(bool);
	int replay_run();
	virtual void setupRunner();
  protected:
	bool shouldRun(bool);
	int runCrestTestMain();
	int runCrest();
	void writeStopFile();
};

class CrestTestInstaller {
  public:
	explicit CrestTestInstaller(
		CrestTestRunner& runner, const char* xmlout, 
		int (*crest_main)(), const char* testname);
	virtual ~CrestTestInstaller();
};

#ifdef CREST_REPLAY
    #define IS_REPLAY_MODE true
#else
    #define IS_REPLAY_MODE false
#endif

#define STRINGIZE(A) #A

#define TEST_CREST(testGroup, testName, start_function) \
class TEST_##testGroup##_##testName##_Auto_TestRunner; \
extern TEST_##testGroup##_##testName##_Auto_TestRunner TEST_##testGroup##_##testName##_Auto_Instance; \
class TEST_##testGroup##_##testName##_Auto_TestRunner : public CrestTestRunner \
{ \
 public: TEST_##testGroup##_##testName##_Auto_TestRunner(): CrestTestRunner() {} \
	void setupRunner(); \
}  TEST_##testGroup##_##testName##_Auto_Instance; \
static CrestTestInstaller TEST_##testGroup##_##testName##_Auto_Installer( \
	TEST_##testGroup##_##testName##_Auto_Instance, \
        STRINGIZE(cpputest_##testGroup##_##testName##_Auto.xml), \
	start_function, \
    #testName); \
TEST(testGroup, testName) { \
	TEST_##testGroup##_##testName##_Auto_Instance.run(IS_REPLAY_MODE); \
} \
void TEST_##testGroup##_##testName##_Auto_TestRunner::setupRunner()

#define CREST_SETUP(crestPath, strategy, iterMax) \
  static class CrestConfigInstaller s_crest_setup(crestPath, strategy, iterMax); 

#endif /* __CREST_TEST_HARNESS_H__ */
=======
#ifndef __CREST_TEST_HARNESS_H__
#define __CREST_TEST_HARNESS_H__

#include <stdio.h>
#include "TestHarness.h"

#define CREST_STRATEGY_DFS "dfs"
#define CREST_STRATEGY_CFG "cfg"
#define CREST_STRATEGY_RANDOM "random"

#define ENV_STRING_ENTRY_CALL_MODE "__ENABLE_CPPUTEST_CREST"
#define ENV_STRING_CREST_STOP "CREST_STOP"
#define ENV_STRING_CREST_ITERATION "CREST_ITERATION"
#define ENV_STRING_CREST_ERROR_LOG "CREST_ERROR_LOG"

extern "C" {
	#include <stdlib.h>	// system, getenv
	extern void __CrestInit(void);
}

class CrestConfig {
  public:
  	static CrestConfig& instance();
	virtual ~CrestConfig() {}
	const char *crest_path_;
	const char *strategy_;
	unsigned int maxIteration_;	
	
  private:
	CrestConfig(const char* crest_path, const char *strategy, unsigned int iter);

};

class CrestConfigInstaller {
  public:
	CrestConfigInstaller(const char *crest_path, const char *strategy, unsigned int iterMax);
	virtual ~CrestConfigInstaller() {};
};

class CrestTestRunner {
  public:
	CrestTestRunner();
	virtual ~CrestTestRunner();
	const char *crest_path_;
	const char *xmloutpath_;
	unsigned int maxIteration_;
	const char *strategy_;
	int (*entry_)();
   	const char *cpputestname_;
	void setMaxIteration(unsigned int n);
	void setStrategy(const char *s);
	void setCrestPath(const char *s);	
	int run(bool);
	int replay_run();
	virtual void setupRunner();
  protected:
	bool shouldRun(bool);
	int runCrestTestMain();
	int runCrest();
	void writeStopFile();
};

class CrestTestInstaller {
  public:
	explicit CrestTestInstaller(
		CrestTestRunner& runner, const char* xmlout, 
		int (*crest_main)(), const char* testname);
	virtual ~CrestTestInstaller();
};

#ifdef CREST_REPLAY
    #define IS_REPLAY_MODE true
#else
    #define IS_REPLAY_MODE false
#endif

#define STRINGIZE(A) #A

#define TEST_CREST(testGroup, testName, start_function) \
class TEST_##testGroup##_##testName##_Auto_TestRunner; \
extern TEST_##testGroup##_##testName##_Auto_TestRunner TEST_##testGroup##_##testName##_Auto_Instance; \
class TEST_##testGroup##_##testName##_Auto_TestRunner : public CrestTestRunner \
{ \
 public: TEST_##testGroup##_##testName##_Auto_TestRunner(): CrestTestRunner() {} \
	void setupRunner(); \
}  TEST_##testGroup##_##testName##_Auto_Instance; \
static CrestTestInstaller TEST_##testGroup##_##testName##_Auto_Installer( \
	TEST_##testGroup##_##testName##_Auto_Instance, \
        STRINGIZE(cpputest_##testGroup##_##testName##_Auto.xml), \
	start_function, \
    #testName); \
TEST(testGroup, testName) { \
	TEST_##testGroup##_##testName##_Auto_Instance.run(IS_REPLAY_MODE); \
} \
void TEST_##testGroup##_##testName##_Auto_TestRunner::setupRunner()

#define CREST_SETUP(crestPath, strategy, iterMax) \
  static class CrestConfigInstaller s_crest_setup(crestPath, strategy, iterMax); 

#endif /* __CREST_TEST_HARNESS_H__ */
>>>>>>> d45aa962a1d87837a2c34bbb35dc101b5dc1f716
