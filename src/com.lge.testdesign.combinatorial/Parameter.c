#include "utils/common.h"

typedef struct List *list_pointer;


typedef struct Parameter {
	String* name;
	List* values;
} Parameter;

Parameter* New_ParameterClass(char* name)
{
	Parameter* parameter = ALLOC(Parameter);
	parameter->name = String_Clone(name);

	return parameter;
}

void	addValue(Parameter* parameter, void* data)
{
	parameter->values = List_Append(parameter->values, data);
}

int length(Parameter* parameter)
{
	return List_Length(parameter->values);
}
