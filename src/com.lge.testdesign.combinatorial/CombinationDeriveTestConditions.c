/*
 *
 * CombinationDeriveTestConditions.c
 */
#include "com.lge.testdesign.combinatorial/Combinatorial.h"

typedef struct CombinationDeriveTestConditions {
	DeriveTestConditions base;
	Set* parameters;

}CombinationDeriveTestConditions;

CombinationDeriveTestConditions* New_CombinationDeriveTestConditions()
{
	CombinationDeriveTestConditions* conds = ALLOC(CombinationDeriveTestConditions);

	// 구현
	conds->parameters = New_Set();

	return conds;
}

void CombinationDeriveTestConditions_put(CombinationDeriveTestConditions* conds, void* parameter)
{
	// 구현
	Set_put(conds->parameters, parameter);
}

Set* CombinationDeriveTestConditions_genTestConditions(CombinationDeriveTestConditions* conds)
{

	return conds->parameters;
}

