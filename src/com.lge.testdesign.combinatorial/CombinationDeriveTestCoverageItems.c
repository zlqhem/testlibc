#include "com.lge.testdesign.combinatorial/Combinatorial.h"

/*
 * @return Set<Map<String, Object>>
 */
Set* CombinationDeriveTestConditions_genTestCoverageItems(
		CombinationDeriveTestCoverageItems* genCoverageItems, Set* testConditions)
{
	return genCoverageItems->base.genTestCoverageItems(genCoverageItems, testConditions);
}
