/*
 * ParameterValuePairContainer.c
 *
 */
#include "com.lge.testdesign.combinatorial/Combinatorial.h"

typedef struct ParameterValuePairContainer {
	char *name;
	void *value;
} ParameterValuePairContainer;


BOOL ParameterValuePairContainer_equal(void *a, void *b)
{
	ParameterValuePairContainer *pa = (ParameterValuePairContainer*)a;
	ParameterValuePairContainer *pb = (ParameterValuePairContainer*)b;

	// TODO: TBD
	return FALSE;
}
