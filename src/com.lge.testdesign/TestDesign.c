#include "com.lge.testdesign/TestDesign.h"

TestDesign* New_TestDesign(TestDesign* td, DeriveTestConditions* conds, DeriveTestCoverageItems* items)
{
	td->deriveTestConditions = conds;
	td->deriveTestCoverageItems = items;
	return td;
}

Set* TestDesign_generateTestConditions(TestDesign* td)
{
	return td->deriveTestConditions->genTestConditions(td->deriveTestConditions);
}

Set* TestDesign_generateCoverageItems(TestDesign* td, Set* testConditions)
{
	DeriveTestCoverageItems* genCoverageItems = td->deriveTestCoverageItems;
	return genCoverageItems->genTestCoverageItems(genCoverageItems, testConditions);
}

Set* TestDesign_deriveCoverageItems(TestDesign* td)
{
	DeriveTestConditions* genTestConditions = td->deriveTestConditions;
	DeriveTestCoverageItems* genTestCoverageItems = td->deriveTestCoverageItems;
	Set* conditions = td->deriveTestConditions->genTestConditions(genTestConditions);

	return genTestCoverageItems->genTestCoverageItems(genTestCoverageItems, conditions);
}

Set* DeriveTestConditions_genTestConditions(DeriveTestConditions* conds)
{
	return conds->genTestConditions(conds);
}
