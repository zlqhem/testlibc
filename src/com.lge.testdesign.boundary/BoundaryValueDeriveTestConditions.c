/*
 * BoundaryValueDeriveTestConditions.c
 *
 *  Created on: 2015. 1. 27.
 *      Author: junhee.cho
 */

#include "utils/common.h"
#include "com.lge.testdesign/TestDesign.h"
#include "com.lge.testdesign.boundary/BoundaryValue.h"

BoundaryValueDeriveTestConditions* New_BoundaryValueDeriveTestConditions()
{
	BoundaryValueDeriveTestConditions* cond = ALLOC(BoundaryValueDeriveTestConditions);
	cond->boundaries = New_Set();
	cond->base.genTestConditions = BoundaryValueDeriveTestConditions_genTestConditions;
	return cond;
}

void BoundaryValueDeriveTestConditions_put(BoundaryValueDeriveTestConditions* conds, void* boundary)
{
	Set_put(conds->boundaries, boundary);
}

Set* BoundaryValueDeriveTestConditions_genTestConditions(BoundaryValueDeriveTestConditions* conds)
{
	return conds->boundaries;
}
