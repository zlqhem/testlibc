#include "utils/common.h"
#include "com.lge.testdesign/TestDesign.h"
#include "com.lge.testdesign.boundary/BoundaryValue.h"

static Set* genTestCoverageItems(DeriveTestCoverageItems* genCoverageItems, Set* testConditions);

BoundaryValueDeriveTestCoverageItems* New_IntegerBoundaryValueDeriveTestCoverageItems()
{
	BoundaryValueDeriveTestCoverageItems* coverageItemgen = New_BoundaryValueDeriveTestCoverageItems();
	coverageItemgen->base.genTestCoverageItems = genTestCoverageItems;
	return coverageItemgen;
}

static Set* genTestCoverageItems(DeriveTestCoverageItems* genCoverageItems, Set* testConditions)
{
	int len = testConditions->len;
	Set* result = New_Set();

	for (int i=0; i<len; i++)
	{
		int boundary = (int)testConditions->data[i];
		Set_put(result, (void*)(boundary));
		Set_put(result, (void*)(boundary-1));
		Set_put(result, (void*)(boundary+1));
	}

	return result;
}

