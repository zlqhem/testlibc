/*
 * BoundaryValueDeriveTestCoverageItems.c
 *
 *  Created on: 2015. 1. 27.
 *      Author: junhee.cho
 */

#include "utils/common.h"
#include "com.lge.testdesign/TestDesign.h"
#include "com.lge.testdesign.boundary/BoundaryValue.h"

BoundaryValueDeriveTestCoverageItems* New_BoundaryValueDeriveTestCoverageItems()
{
	BoundaryValueDeriveTestCoverageItems* coverageItemgen = ALLOC(BoundaryValueDeriveTestCoverageItems);
	return coverageItemgen;
}

Set* BoundaryValueDeriveTestCoverageItems_genTestCoverageItems(
		BoundaryValueDeriveTestCoverageItems* genCoverageItems, Set* testConditions)
{
	return genCoverageItems->base.genTestCoverageItems(genCoverageItems, testConditions);
}


