/*
 * common.c
 *
 *  Created on: 2015. 1. 30.
 *      Author: junhee.cho
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>
#include "utils/common.h"

Pair* New_Pair(void* first, void* second)
{
	Pair* pair = ALLOC(Pair);
	pair->first = first;
	pair->second = second;
	return pair;
}

List*	List_Create(void* data)
{
	List* r = (List*)calloc(1, sizeof(List));
	r->next = NULL;
	r->data = data;
	return r;
}

List*	List_Append(List* list, void* data)
{
	List *p = list;
	if (NULL == p) return List_Create(data);

	while (NULL != p)
	{
		if (NULL == p->next)
		{
			p->next = List_Create(data);
			break;
		}
		else
		{
			p = p->next;
		}
	}
	assert (p->next);
	return list;
}

List*  findLastNode(List* list)
{
	while (NULL != list)
	{
		if (NULL == list->next)
			return list;

		list = list->next;
	}
	return NULL;
}

List*	List_Concat(List* l1, List* l2)
{
	List* lastNode = findLastNode(l1);
	if (NULL == lastNode) return l2;

	lastNode->next = l2;
	return l1;
}

void 	List_Free(List* list)
{
	while (NULL != list)
	{
		List* next = list->next;
		free (list);
		list = next;
	}
}

int		List_Length(List* list)
{
	int cnt = 0;
	while (NULL != list)
	{
		list = list->next;
		++cnt;
	}
	return cnt;
}

void    List_Iterate(List* list, void (*apply)(void* elem))
{
    List* p = list;

    for (; p != NULL; p = p->next)
    {
        apply(p->data);
    }
}


String*  String_Clone(const char* s)
{
    String* ret = (String*)calloc(1, sizeof(String));
    snprintf (ret->str, MAX_INPUT_STR_LEN, "%s", s);
    return ret;
}

String*  String_Clone_Int(int i)
{
    String* ret = (String*)calloc(1, sizeof(String));
    snprintf (ret->str, MAX_INPUT_STR_LEN, "%d", i);
    return ret;
}

void    String_Free(String* s)
{
    if (s) free(s);
}

/*
 * requires (NULL != s && NULL != s2)
 */
int     String_CompareTo(const String* s, const String* s2)
{
    assert(s);
    assert(s2);
    return strcmp(s->str, s2->str);
}

int     String_CompareToRaw(const String* s, const char* s2)
{
    return strcmp(s->str, s2);
}

String* String_CopyRaw(String* out, const char* s)
{
    assert(out);
    snprintf (out->str, MAX_INPUT_STR_LEN, "%s", s);
    return out;
}

String* String_Copy(String* out, const String* s)
{
    assert(out);
    assert(s);
    return String_CopyRaw(out, s->str);
}

String* String_CopyInt(String* out, int v)
{
    assert(out);
    snprintf (out->str, MAX_INPUT_STR_LEN, "%d", v);
	return out;
}

int     String_ToInt(const String* s)
{
    assert(s);
    return atoi(s->str);
}

int     String_Strlen(String* s)
{
    assert(s);
    return strlen(s->str);
}

BOOL    String_IsEmpty(const String* s)
{
    return '\0' == *s->str;
}
