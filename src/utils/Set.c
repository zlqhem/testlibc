#include <assert.h>
#include "utils/common.h"
#include "utils/Set.h"

Set* New_Set()
{
	Set* alloc = ALLOC(Set);
	return alloc;
}

void Set_put(Set* set, void* elem)
{
	assert (set != 0);
	set->data[set->len++] = elem;
}

void Set_add(Set* set, void* elem)
{
	assert (set != 0);
	set->data[set->len++] = elem;
}

BOOL Set_isEmpty(Set* set)
{
	assert (set != 0);
	return (set->len == 0) ? TRUE: FALSE;
}

void Set_addAll(Set* dest, Set* src)
{
	assert (dest != 0);
	for (int i=0; i<src->len; i++)
	{
		Set_add(dest, src->data[i]);
	}
}

void Set_remove(Set* set, void* elem)
{
	int i;
	BOOL removed = FALSE;
	assert (set != 0);
	for (i=0; i<set->len; i++)
	{
		if (set->data[i] == elem)
		{
			removed = TRUE;
			break;
		}
	}

	// i+1, i+2, ... 를 한칸씩 앞으로 이동시키기
	for (int k=i+1; k<set->len; k++)
	{
		set->data[k-1] = set->data[k];
	}

	if (removed == TRUE)
	{
		set->len--;
	}

}

BOOL Set_equals_cmp(Set* src, Set* target, int (*cmp)(void*, void*))
{
	if (src->len != target->len)
		return FALSE;

	for (int i=0; i<src->len; i++)
	{
		int found = FALSE;

		for (int j=0; j<target->len; j++)
		{
			if (0 == cmp(src->data[i], target->data[j]))
			{
				found = TRUE;
				break;
			}
		}

		if (found == FALSE)
		{
			return FALSE;
		}
	}
	return TRUE;
}

static BOOL raw_eq(void* a, void* b)
{
	return a == b ? TRUE : FALSE;
}

BOOL Set_equals(Set* src, Set* target)
{
	return Set_equals_cmp(src, target, raw_eq);
}

void Free_Set(Set* set)
{
	if (set)
	{
		free(set);
	}
}

int Set_length(Set* set)
{
	assert (0 != set);
	return set->len;
}
