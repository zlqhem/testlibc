/*
 * PartitionOp.c
 *
 *  Created on: 2015. 1. 30.
 *      Author: junhee.cho
 */
#include <assert.h>
#include "com.lge.testdesign.partition/EquivalencePartition.h"

static BOOL or(void* env, void* elem);
static BOOL and(void* env, void* elem);
static BOOL negate(void* env, void* elem);
static BOOL emptyPartitionClass(void* env, void* elem);

PartitionClass* PartitionOp_or(PartitionClass* a, PartitionClass* b)
{
	Pair* arguments = New_Pair(a, b);

	return New_PartitionClass(arguments, or);
}

PartitionClass* PartitionOp_and(PartitionClass* a, PartitionClass* b)
{
	Pair* arguments = New_Pair(a, b);

	return New_PartitionClass(arguments, and);
}

PartitionClass* PartitionOp_negate(PartitionClass* basis)
{
	return New_PartitionClass(basis, negate);
}

PartitionClass* PartitionOp_nothing()
{
	return New_PartitionClass(NULL, emptyPartitionClass);
}

static BOOL or(void* env, void* elem)
{
	Pair* pair = (Pair*)env;
	PartitionClass* a = pair->first;
	PartitionClass* b = pair->second;

	return PartitionClass_contains(a, elem) || PartitionClass_contains(b, elem);
}

static BOOL and(void* env, void* elem)
{
	Pair* pair = (Pair*)env;
	PartitionClass* a = pair->first;
	PartitionClass* b = pair->second;

	return PartitionClass_contains(a, elem) && PartitionClass_contains(b, elem);
}

static BOOL negate(void* env, void* elem)
{
	PartitionClass* partition = (PartitionClass*)env;
	return !PartitionClass_contains(partition, elem);
}

static BOOL emptyPartitionClass(void* env, void* elem)
{
	return FALSE;
}

