#include <stdarg.h>
#include "com.lge.testdesign/TestDesign.h"
#include "com.lge.testdesign.partition/EquivalencePartition.h"

typedef struct EPDeriveTestCoverageItems {
	DeriveTestCoverageItems base;
} EPDeriveTestCoverageItems;

static Set* genTestCoverageItems (Set* testConditions);

EPDeriveTestCoverageItems* New_EPDeriveTestCoverageItems()
{
	EPDeriveTestCoverageItems* obj = ALLOC(EPDeriveTestCoverageItems);
	obj->base.genTestCoverageItems = genTestCoverageItems;
	return obj;
}

Set* EPDeriveTestCoverageItems_genTestCoverageItems(
		EPDeriveTestCoverageItems* coverageItemgen, Set* testConditions)
{
	return coverageItemgen->base.genTestCoverageItems(coverageItemgen, testConditions);
}

/*
 * @param testConditions set of PartitionClass
 * @return set of arbitrary type
 *
 */
static Set* genTestCoverageItems (Set* testConditions)
{
	// set of PartitionClass
	Set* coverageItems = New_Set();
	Set_addAll(coverageItems, testConditions);

	Set* result = New_Set();
	return NULL;
}
