/*
 * EPDeriveTestConditions.c.c
 *
 *  Created on: 2015. 1. 27.
 *      Author: junhee.cho
 */

#include <assert.h>
#include "utils/common.h"
#include "utils/Set.h"
#include "com.lge.testdesign/TestDesign.h"
#include "com.lge.testdesign.partition/EquivalencePartition.h"

typedef struct EPDeriveTestConditions {
	DeriveTestConditions 	base;
	PartitionClass* 		allPartitions;
	Set* 					validDomains;	// set of PartitionClass
} EPDeriveTestConditions;

Set* EPDeriveTestConditions_genTestConditions(EPDeriveTestConditions* this);

static void addPartition(EPDeriveTestConditions* this, PartitionClass* partition);
static PartitionClass* getInvalidPartition(EPDeriveTestConditions* this);

EPDeriveTestConditions* New_EPDeriveTestConditions()
{
	EPDeriveTestConditions* cond = ALLOC(EPDeriveTestConditions);
	cond->base.genTestConditions = EPDeriveTestConditions_genTestConditions;
	cond->allPartitions = PartitionOp_nothing();
	cond->validDomains = New_Set();
	return cond;
}

void EPDeriveTestConditions_put(EPDeriveTestConditions* this, PartitionClass* pc)
{
	assert(0);
}

void EPDeriveTestConditions_putIntRange(EPDeriveTestConditions* this, int start, int end)
{
	addPartition(this, New_PartitionClass_Range(start, end));
}

static void addPartition(EPDeriveTestConditions* this, PartitionClass* partition)
{
	this->allPartitions = PartitionOp_or(this->allPartitions, partition);
	Set_add(this->validDomains, partition);
}

// return set of PartitionClass
Set* EPDeriveTestConditions_genTestConditions(EPDeriveTestConditions* this)
{
	Set* partitioned = New_Set();
	Set_addAll(partitioned, this->validDomains);

	// valid partition 이 없다면, invalid partition 도 고려하지 않음.
	if (TRUE != Set_isEmpty(this->validDomains))
	{
		Set_add(partitioned, getInvalidPartition(this));
	}
	return partitioned;
}

static PartitionClass* getInvalidPartition(EPDeriveTestConditions* this)
{
	return PartitionOp_negate(this->allPartitions);
}

