#include "utils/common.h"
#include "com.lge.testdesign.partition/EquivalencePartition.h"

typedef struct Range {
	int start;
	int end;
} Range;

static BOOL integerContains(void* env, void* elem);

BOOL PartitionClass_contains(PartitionClass* partitionClass, void* element)
{
	return partitionClass->contains(partitionClass->env, element);
}

PartitionClass* New_PartitionClass(void* env, BOOL (*contains)(void* env, void* elem))
{
	PartitionClass* obj = ALLOC(PartitionClass);
	obj->env = env;
	obj->contains = contains;
	return obj;
}

PartitionClass* New_PartitionClass_Range(int start, int end)
{
	PartitionClass* partitionClass = ALLOC(PartitionClass);
	Range* env = ALLOC(Range);
	env->start = start;
	env->end = end;

	partitionClass->env = env;
	partitionClass->contains = integerContains;
	return partitionClass;
}

static BOOL integerContains(void* env, void* elem)
{
	Range* r = (Range*)env;
	int element = CAST_INT(elem);
	return (element >= r->end && element <= r->start) ? TRUE : FALSE;
}
